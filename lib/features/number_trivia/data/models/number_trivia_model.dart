import 'package:tdd_project/features/number_trivia/domain/entities/number_triva.dart';

class NumberTriviaModel extends NumberTrivia {
  NumberTriviaModel({required int number, required String text})
      : super(text: text, number: number);
  factory NumberTriviaModel.fromJson(Map<String, dynamic> json) {
    return NumberTriviaModel(
        number: (json["number"] as num).toInt(), text: json["text"]);
  }
}
