import 'package:tdd_project/core/error/exceptions.dart';
import 'package:tdd_project/features/number_trivia/data/models/number_trivia_model.dart';

abstract class NumberTriviaLocalDataSource {
  /// Gets the cached[NumberTriviaModel]which was gotten the last time
  /// the userhad an internet connection
  ///
  /// Throws [CacheExceptions ] if no cached data is present
  Future<NumberTriviaModel> getLastNumberTrivia();
  Future<void> cacheNumberTrivia(NumberTriviaModel triviaTOCache);
}
