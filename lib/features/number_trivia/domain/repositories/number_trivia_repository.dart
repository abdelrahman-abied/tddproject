import 'package:dartz/dartz.dart';
import 'package:tdd_project/core/error/failures.dart';
import 'package:tdd_project/features/number_trivia/domain/entities/number_triva.dart';

abstract class NumberTriviaRepository {
  Future<Either<Failure, NumberTrivia>> getConcreteNumberTrivia(int? number);
  Future<Either<Failure, NumberTrivia>?> getRandomNumberTrivia();
}
