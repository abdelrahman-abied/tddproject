import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:tdd_project/core/error/failures.dart';
import 'package:tdd_project/core/usecases/usecase.dart';
import 'package:tdd_project/features/number_trivia/domain/entities/number_triva.dart';
import 'package:tdd_project/features/number_trivia/domain/repositories/number_trivia_repository.dart';

class GetConcreteNumberTrivia implements UseCase<NumberTrivia, Params> {
  late final NumberTriviaRepository repository;
  GetConcreteNumberTrivia(this.repository);
  @override
  Future<Either<Failure, NumberTrivia>> call(Params params) async {
    final numberTrivia =
        await repository.getConcreteNumberTrivia(params.number);
    return numberTrivia;
  }
}

class Params extends Equatable {
  final int number;

  Params({required this.number});

  @override
  //  implement props
  List<Object?> get props => [number];
}
