import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:tdd_project/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:tdd_project/features/number_trivia/domain/entities/number_triva.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tNumberTriviaModel = NumberTriviaModel(number: 1, text: "Test text");

  test('should be subclass of NumberTrivia entity', () async {
    expect(tNumberTriviaModel, isA<NumberTrivia>());
  });
  group('fromjson', () {
    test('should return a vaild model when json number is an integer',
        () async {
      final Map<String, dynamic> jsonMap = json.decode(fixture("trivia.json"));
      final NumberTriviaModel result = NumberTriviaModel.fromJson(jsonMap);
      print(result.number);
      expect(result, tNumberTriviaModel);
    });
    test('should return a vaild model when json number is a double', () async {
      final Map<String, dynamic> jsonMap =
          json.decode(fixture("trivia_double.json"));
      final NumberTriviaModel result = NumberTriviaModel.fromJson(jsonMap);
      print(result.number);
      expect(result, tNumberTriviaModel);
    });
  });
}
